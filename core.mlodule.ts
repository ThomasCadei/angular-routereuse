import { RouteReuseStrategy } from '@angular/router';
import { RouteReuseService } from './services/route-reuse.service';

@NgModule({
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: RouteReuseService
    }
  ]
})
export class CoreModule {}