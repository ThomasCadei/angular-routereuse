import { RouteReuseStrategy, ActivatedRouteSnapshot, DetachedRouteHandle } from '@angular/router';
import { ComponentRef } from '@angular/core';

export class RouteReuseService implements RouteReuseStrategy {

  public static handlers: { [key: string]: DetachedRouteHandle } = {};

  private static getRouteUrl(route: ActivatedRouteSnapshot) {
    return route['_routerState'].url.replace(/[\/, -]/g, '_');
  }

  /** Triggered when the route leaves. Store route snapshots by path as key & component current instance object */
  public store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
    RouteReuseService.handlers[RouteReuseService.getRouteUrl(route)] = handle;
  }

  /** If path is considered to be allowed to restore routing in the cache */
  public shouldAttach(route: ActivatedRouteSnapshot): boolean {
    return !!route.routeConfig && !!RouteReuseService.handlers[RouteReuseService.getRouteUrl(route)];
  }

  /** Represents that multiplexing is allowed for all routes. If you have routes that you don't want to use, you can add some business logic here. */
  public shouldDetach(route: ActivatedRouteSnapshot): boolean {
    if (!route.routeConfig || route.routeConfig.loadChildren) {
      return false;
    }
    return true;
  }

  /** Get a snapshot from the cache and return nul if not */
  public retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
    if (!route.routeConfig) {
      return null;
    }
    if (route.routeConfig.loadChildren) return null;
    return RouteReuseService.handlers[RouteReuseService.getRouteUrl(route)];
  }

  /** Enter the routing trigger to determine whether the same route is available */
  public shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    return future.routeConfig === curr.routeConfig;
  }

  /** Clear cache */
  public clearCacheHandle() {
    for (const key in RouteReuseService.handlers) {
      if (RouteReuseService.handlers[key]) {
        this.deactivateOutlet(RouteReuseService.handlers[key])
      }
    }
  }

  /** Handling the problem of different links to the same component */
  private deactivateOutlet(handle: DetachedRouteHandle): void {
    const componentRef: ComponentRef<any> = handle ? handle['componentRef'] : null;
    if (componentRef) {
      componentRef.destroy();
    }
  }

  public clearCacheByUrl(url: string) {
    const handleUrl = url.replace(/[\/, -]/g, '_');
    for (const key in RouteReuseService.handlers) {
      if (key === handleUrl) {
        delete RouteReuseService.handlers[key];
      }
    }
  }

}